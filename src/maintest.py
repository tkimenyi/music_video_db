from java.lang import System, Class, ClassNotFoundException
from java.sql import DriverManager, SQLException
from java.awt import Desktop
from java.io import IOException
from java.net import URISyntaxException 
from java.net import URI
import sys


  

class Data:
    def __init__(self):
        try:
            System.setProperty("sqlite.purejava", "true") # Strictly optional; use if you get UnsatisfiedLinkError or the like.
            Class.forName("org.sqlite.JDBC")
            connection = DriverManager.getConnection("jdbc:sqlite:favorites")
            self.statement = connection.createStatement()    
    
        except ClassNotFoundException, e:
            sys.exit(e.message)
                     
        except SQLException, e:
            sys.exit(e.message)
                      
        self.createInitialDatabase()
        
    def createInitialDatabase(self):
        print 'Database ready to be created' 

        createSongs = "CREATE TABLE if not exists songs(song_name TEXT, link TEXT, artist_name TEXT, album_name TEXT) "      
        createArtists = "CREATE TABLE if not exists artists(artist_name TEXT, artist_id INT)"
        createAlbums = "CREATE TABLE if not exists albums(album_name TEXT, artist_name TEXT, genre TEXT, year INT)"

        self.statement.addBatch(createSongs)
        self.statement.addBatch(createArtists)
        self.statement.addBatch(createAlbums)
        self.statement.executeBatch()        
        
        for t in [('hey mama','http://www.youtube.com/watch?v=RtpKqMpqGU0','kanye west','college dropout' ),
                  ('the new workout plan','http://www.youtube.com/watch?v=Ua61XY84gGg&ob=av2e','kanye West','college dropout'),
                  ('through the wire','http://www.youtube.com/watch?v=uvb-1wjAtk4','kanye West','college dropout'),
                  ('jesus walks','http://www.youtube.com/watch?v=b8AyHupByuU','kanye west','college dropout'),
                  ('all falls down','http://www.youtube.com/watch?v=8kyWDhB_QeI&ob=av2e','kanye west','college dropout'),
                  ('spaceship','http://www.youtube.com/watch?v=wGM6N0qXeu4','kanye west','college dropout'),
                  ('never let me down','http://www.youtube.com/watch?v=9sHpHQTuZC4','kanye West','college dropout'),
                  ('family business','http://www.youtube.com/watch?v=z8yoxyIu0s8','kanye west','college dropout'),
                  ('last call','http://www.youtube.com/watch?v=V9mwuYBljUA','kanye west','college dropout'),
                  ('the scientist','http://www.youtube.com/watch?v=EqWLpTKBFcU&ob=av2e','coldplay','a rush of blood to the head'),
                  ('in my place','http://www.youtube.com/watch?v=yEoHFzEmld0&ob=av2e', 'coldplay','a rush of blood to the head'),
                  ('god put a smile on my face','http://www.youtube.com/watch?v=e9Kcg_8gK30&ob=av2e','coldplay','a rush of blood to the head'),
                  ('clocks','http://www.youtube.com/watch?v=d020hcWA_Wg&ob=av2n','coldplay','a rush to the blood to the head'),
                  ('yellow','http://www.youtube.com/watch?v=1MwjX4dG72s&ob=av2e','coldplay','parachutes',),
                  ('fix you','http://www.youtube.com/watch?v=JI-o25K6B-E&ob=av2e','coldplay','x&y'),
                  ('speed of sound','http://www.youtube.com/watch?v=TahH7B_aUZc&ob=av2n','coldplay','x&y'),
                  ('the hardest part','http://www.youtube.com/watch?v=1Tp0r9197uo','coldplay','x&y'),
                  ('talk','http://www.youtube.com/watch?v=W0uqLM1uj_k&ob=av2e','coldplay','x&y'),
                  ('lost','http://www.youtube.com/watch?v=7HkjGg_pPJU&ob=av2n','coldplay','viva la vida'),
                  ('thriller','http://www.youtube.com/watch?v=sOnqjkJTMaA&ob=av2n','michael jackson','thriller'),
                  ('billie jean','http://www.youtube.com/watch?v=Zi_XLOBDo_Y&ob=av2n','michael jackson','thriller'),
                  ('beat it','http://www.youtube.com/watch?v=oRdxUFDoQe0&ob=av2e','michael jackson','thriller'),
                  ('one love', 'http://www.youtube.com/watch?v=vdB-8eLEW8g','bob marley', 'the very best of bob marley'),
                  ('three little birds','http://www.youtube.com/watch?v=kIjkW6iyXNo', 'bob marley', 'the very best of bob marley'),
                  ('no woman no cry','http://www.youtube.com/watch?v=n4kpqDF9j6Q','bob marley','the very best of bob marley'),
                  ('homecoming', 'http://www.youtube.com/watch?v=LQ488QrqGE4&feature=relmfu', 'kanye west','graduation'), ]:
            self.statement.execute("INSERT INTO songs VALUES" + "('" + t[0]+ "','" + t[1] + "','" + t[2] + "','" + t[3] + "')")
                        
    def addSong(self,song, link, artist, album):        
        self.statement.execute("INSERT INTO songs VALUES" + "('" + song+ "','" + link + "','" + artist + "','" + album + "')")
        rs = self.statement.executeQuery("SELECT * FROM songs") 
    
        columns = 4
        while rs.next():
            for c in range(1, columns + 1):
                print rs.getString(c), '\t',
            print

    def searchSong (self,song):       
        rs = self.statement.executeQuery("SELECT song_name FROM songs WHERE artist_name = '" +song.lower()+ "'")       
        columns = 1
        results = []
        while rs.next():
            for c in range(columns, columns+1):
                results.append(rs.getString(c) + " ")
        return results
             
    def openSong (self, song):            
        openS = self.statement.executeQuery("SELECT link FROM songs WHERE artist_name = '"+ song.lower() + "' OR song_name = '"+ song.lower() + "'")
        columns = 1
        for c in range(1,columns+1):           
            link = openS.getString(c)
            print("Should break")
            break
        
        try:  
            if Desktop.isDesktopSupported():
                desktop = Desktop.getDesktop()
                uri = URI(link)
                desktop.browse(uri)
        except IOException, e:
            e.printStackTrace(e)
        
        except URISyntaxException, e:
            e.printStackTrace(e)  
    
    def checkIfExists(self, song):           
        openS = self.statement.executeQuery("SELECT song_name FROM songs WHERE song_name = '"+ song.lower() + "'" )
        columns = 1   
                 
        for c in range(1,columns+1):         
            if song.lower() == openS.getString(c):
                print ' it exists '    
                return True
                