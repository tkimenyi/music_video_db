from javax.swing import JButton
from javax.swing import JFrame
from javax.swing import JPanel
from javax.swing import JTextField
from javax.swing import JMenu
from javax.swing import JMenuBar
from javax.swing import JMenuItem
from javax.swing import JComboBox
from javax.swing import JList
from java.awt import Color

from java.awt.event import KeyListener, ActionListener

from maintest import Data
from AddSong import AddFrame
from RemoveSong import RemoveFrame

class GUI(JFrame):
    def __init__(self):
        super(GUI, self).__init__()
        self.initUI()        

    def initUI(self):           
        self.panel = JPanel()                
        self.artists = [ "Bob Marley", "Coldplay", "Kanye West", "Michael Jackson" ]       
        self.menu = Menu()               
        self.searchbar = SearchBar()       
        self.getter = ArtistGetter(self)
        self.artistmenu = JComboBox(self.artists)
        self.artistmenu.setSelectedItem(None)
        self.artistmenu.addActionListener(self.getter)
        self.artistmenu.setBounds(100, 60, 350, 30)
        self.panel.add(self.artistmenu)              
        self.getContentPane().add(self.panel)
        self.setJMenuBar(self.menu.makeMe())       
        self.panel.add(self.searchbar.makeMe())        
        button = JButton("Search", actionPerformed= self.searchbar.getSearch)
        button.setBounds(370, 30, 100, 30)
        self.panel.add(button)       
        self.panel.setLayout(None)       
        self.setTitle("Favorite Song Database")
        self.setSize(615, 400)
        self.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        self.setLocationRelativeTo(None)
        self.setVisible(True)
        self.setResizable(False)       
        self.panel.setBackground(Color.GREEN)
    
        
 
    
class ArtistGetter(ActionListener):
    def __init__(self, frame):
        self.outer = frame
    
    def actionPerformed(self, e):
        artist = self.outer.artistmenu.getSelectedItem()
        songs = database.searchSong(artist)            
        output = songs                  
        
              
        search = makeOutputList()
        
        song = search.makeSong1()
        song.setEditable(False) 
        song.setText(output[0])
        
        artist = search.makeSong2()
        artist.setEditable(False)
        artist.setText(output[1])
        
        link = search.makeSong3()
        link.setEditable(False)
        link.setText(output[2]) 
        
        album = search.makeSong4()
        album.setEditable(False)
        album.setText(output[3])
            
        gui.panel.add(song) 
        gui.panel.add(artist)
        gui.panel.add(link)
        gui.panel.add(album)
        
        songbutton = JButton("Play", actionPerformed= search.getSong1)
        songbutton.setBounds(370, 110, 100, 30)
        gui.panel.add(songbutton)
        
        artistbutton = JButton("Play", actionPerformed= search.getSong2)
        artistbutton.setBounds(370, 150, 100, 30)
        gui.panel.add(artistbutton)
        
        linkbutton = JButton("Play", actionPerformed= search.getSong3)
        linkbutton.setBounds(370, 190, 100, 30)
        gui.panel.add(linkbutton)
        
        albumbutton = JButton("Play", actionPerformed= search.getSong4)
        albumbutton.setBounds(370, 230, 100, 30)
        gui.panel.add(albumbutton)
      
        #database.openSong(artist)      
        
        

class Menu(JMenuBar):
    def __init__(self):        
        self.menu = JMenuBar()
        self.file = JMenu('File')
        self.add = JMenuItem('Add')
        self.adder = AddSong(self)        
        self.add.addActionListener(self.adder)
        self.remove = JMenuItem('Remove')
        self.remover = RemoveSong(self)        
        self.remove.addActionListener(self.remover)
        
    def makeMe(self):       
        self.menu.add(self.file)
        self.file.add(self.add)
        self.file.add(self.remove)
        return self.menu   
           


class AddSong(ActionListener):
    def __init__(self, add): 
        self.outer = add
    
    def actionPerformed(self, e):            
        AddFrame()
        
        
class RemoveSong(ActionListener):
    def __init__(self, add): 
        self.outer = add
    
    def actionPerformed(self, e):            
        RemoveFrame()    
      

    
class SearchBar(JTextField, KeyListener):    
    def __init__(self):         
        self.searchbar = JTextField()              
       
    def makeMe(self):                       
        self.searchbar.setBounds(100, 30, 270, 30)         
        return self.searchbar      

    def getSearch(self, e):
        song = self.searchbar.getText()     
        database.openSong(song)  
        

        
 
class makeOutputList(JTextField, KeyListener):    
    def __init__(self):         
        self.song1 = JTextField() 
        self.song2 = JTextField()
        self.song3 = JTextField() 
        self.song4 = JTextField()             
       
    def makeSong1(self):                       
        self.song1.setBounds(100, 110, 270, 30)         
        return self.song1
           
    def makeSong2(self):                       
        self.song2.setBounds(100, 150, 270, 30)         
        return self.song2 
           
    def makeSong3(self):                       
        self.song3.setBounds(100, 190, 270, 30)         
        return self.song3    
           
    def makeSong4(self):                       
        self.song4.setBounds(100, 230, 270, 30)         
        return self.song4  
    
    def getSong1(self, e):
        song1 = self.song1.getText()
        print(song1)
        database.openSong(song1)
        
    def getSong2(self, e):
        song2 = self.song2.getText()
        print(song2)
        database.openSong(song2)
        
    def getSong3(self, e):
        song3 = self.song3.getText()
        print(song3)
        database.openSong(song3)
        
    def getSong4(self, e):    
        song4 = self.song4.getText() 
        print(song4)      
        database.openSong(song4)         
        
          
        
if __name__ == '__main__':
    gui = GUI()
    database = Data()            