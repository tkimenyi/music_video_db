from javax.swing import JButton
from javax.swing import JFrame
from javax.swing import JPanel
from javax.swing import JTextField
from java.awt import Color

from java.awt.event import KeyListener

class RemoveFrame(JFrame):
    def __init__(self):
        super(RemoveFrame, self).__init__()
        self.initUI()        

    def initUI(self):           
        self.panel = JPanel()
        
        self.removebar = RemoveBar()     
        self.panel.add(self.removebar.makeMe())
        
        button = JButton("Remove", actionPerformed= self.removebar.removeSong)
        button.setBounds(370, 90, 100, 30)        
        self.panel.add(button)
        
        self.getContentPane().add(self.panel)
        self.panel.setLayout(None)       
        self.setTitle("Remove a Song")
        self.setSize(615, 400)
        self.setLocationRelativeTo(None)
        self.setVisible(True)
        self.setResizable(False)       
        self.panel.setBackground(Color.RED)
        
        
class RemoveBar(JTextField, KeyListener):    
    def __init__(self):         
        self.removebar = JTextField()              
       
    def makeMe(self):                       
        self.removebar.setBounds(100, 90, 270, 30)         
        return self.removebar      

    def removeSong(self, e):
        song = self.removebar.getText()     
        print(song)
        
        
if __name__ == '__main__':   
    RemoveFrame() 