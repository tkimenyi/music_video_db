from javax.swing import JButton
from javax.swing import JFrame
from javax.swing import JPanel
from javax.swing import JTextField
from java.awt import Color

from java.awt.event import KeyListener

class AddFrame(JFrame):
    def __init__(self):
        super(AddFrame, self).__init__()
        self.initUI()        

    def initUI(self):           
        self.panel = JPanel()        
     
        self.songbar = SearchBar()
        self.artistbar = SearchBar() 
        self.linkbar = SearchBar() 
        self.albumbar = SearchBar() 
             
        self.panel.add(self.songbar.makeSong()) 
        self.panel.add(self.artistbar.makeArtist())
        self.panel.add(self.linkbar.makeLink())
        self.panel.add(self.albumbar.makeAlbum())
        
        songbutton = JButton("Song", actionPerformed= self.songbar.getSong)
        songbutton.setBounds(370, 90, 100, 30)
        self.panel.add(songbutton)
        
        artistbutton = JButton("Artist", actionPerformed= self.artistbar.getArtist)
        artistbutton.setBounds(370, 130, 100, 30)
        self.panel.add(artistbutton)
        
        linkbutton = JButton("Link", actionPerformed= self.linkbar.getLink)
        linkbutton.setBounds(370, 170, 100, 30)
        self.panel.add(linkbutton)
        
        albumbutton = JButton("Album", actionPerformed= self.albumbar.getAlbum)
        albumbutton.setBounds(370, 210, 100, 30)
        self.panel.add(albumbutton)
        
        addbutton = JButton("Add Song", actionPerformed = self.songbar.getAll)
        addbutton.setBounds(227, 250, 130, 30)
        self.panel.add(addbutton)
        
        
        self.getContentPane().add(self.panel)
        self.panel.setLayout(None)       
        self.setTitle("Add a Song")
        self.setSize(615, 400)       
        self.setLocationRelativeTo(None)
        self.setVisible(True)
        self.setResizable(False)       
        self.panel.setBackground(Color.BLUE)
        



class SearchBar(JTextField, KeyListener):    
    def __init__(self):         
        self.songbar = JTextField() 
        self.artistbar = JTextField()
        self.linkbar = JTextField() 
        self.albumbar = JTextField()             
       
    def makeSong(self):                       
        self.songbar.setBounds(100, 90, 270, 30)         
        return self.songbar
           
    def makeArtist(self):                       
        self.artistbar.setBounds(100, 130, 270, 30)         
        return self.artistbar 
           
    def makeLink(self):                       
        self.linkbar.setBounds(100,170, 270, 30)         
        return self.linkbar    
           
    def makeAlbum(self):                       
        self.albumbar.setBounds(100, 210, 270, 30)         
        return self.albumbar          

    def getSong(self, e):
        song = self.songbar.getText()
        print song
        
    def getArtist(self, e):
        artist = self.artistbar.getText()
        print artist
        
    def getLink(self, e):
        link = self.linkbar.getText()
        print link
        
    def getAlbum(self, e):    
        album = self.albumbar.getText()       
        print album
    
    def getAll(self, e):
        song = self.songbar.getText()
        artist = self.artistbar.getText()
        link = self.linkbar.getText()
        album = self.albumbar.getText()
        print(song)
        print(artist)
        print(link)
        print(album)

 
if __name__ == '__main__':   
    AddFrame()         